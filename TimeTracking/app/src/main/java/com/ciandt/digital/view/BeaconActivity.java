package com.ciandt.digital.view;

import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;
import com.ciandt.digital.R;
import com.estimote.sdk.Beacon;
import com.estimote.sdk.BeaconManager;
import com.estimote.sdk.Region;
import com.estimote.sdk.Utils;
import java.util.List;
import java.util.UUID;

public class BeaconActivity extends Activity {

    private BeaconManager beaconManager;
    private Region region;
    private TextView text;
    public String txt;

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        beaconManager.stopRanging(region);
        super.onPause();
    }

    private double getDistance(Beacon beacon){
        return Utils.computeAccuracy(beacon);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_beacon);
        text =  (TextView)findViewById(R.id.txtCSharp);
        Button btnMonitoring = (Button)findViewById(R.id.btnMonitoring);
        Button btnRanging = (Button)findViewById(R.id.btnMonitoring);

        if (checkBluetooth()) {
            beaconManager = new BeaconManager(getApplicationContext());
            region = new Region(
                    "monitored region",
                    UUID.fromString("B9407F30-F5F8-466E-AFF9-25556B57FE6D"),
                    null, null);

            /*beaconManager.setMonitoringListener(new BeaconManager.MonitoringListener() {
                @Override
                public void onEnteredRegion(Region region, List<Beacon> list) {
                    Log.e("onEnteredRegion", "ENTROU NA REGIAO");
                }

                @Override
                public void onExitedRegion(Region region) {
                    Log.e("onEnteredRegion", "SAIU DA REGIAO");
                }
            });*/

            beaconManager.setRangingListener(new BeaconManager.RangingListener() {
                @Override
                public void onBeaconsDiscovered(Region region, final List<Beacon> beacons) {
                    if (beacons.size() > 0) {
                        Log.e("RangingListener", "ACHOU! - dist: " + getDistance(beacons.get(0)));
                    }
                }
            });

            beaconManager.stopMonitoring(region);
            beaconManager.stopRanging(region);
            beaconManager.disconnect();
            beaconManager.connect(new BeaconManager.ServiceReadyCallback() {
                @Override
                public void onServiceReady() {
                    beaconManager.startMonitoring(region);
                    beaconManager.startRanging(region);
                }
            });

        }
    }


    public boolean checkBluetooth() {
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter != null && mBluetoothAdapter.isEnabled()) {
            return true;
        } else {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setTitle("Sem Bluetooth");
            alertDialogBuilder.setMessage("o TT precisa do dente azul!");
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }
        return false;
    }

    public String BeaconsInfo(List<Beacon> beacons) {
        StringBuilder beaconsInfo = new StringBuilder();
        beaconsInfo.append("Beacons (");
        beaconsInfo.append(String.valueOf(beacons.size()) + ") ");

        for (Beacon beacon : beacons) {
            double distance = Utils.computeAccuracy(beacon);

            beaconsInfo.append(distance);
            beaconsInfo.append("\n");
        }
        return  beaconsInfo.toString();
    }


}
