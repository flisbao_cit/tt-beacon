package com.ciandt.digital.receiver;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;

import com.ciandt.digital.R;
import com.ciandt.digital.constants.AppConstants;
import com.ciandt.digital.helpers.SharedPreferencesHelper;
import com.ciandt.digital.utils.Utils;
import com.ciandt.digital.view.MainActivity;
import com.estimote.sdk.Beacon;
import com.estimote.sdk.BeaconManager;
import com.estimote.sdk.Region;

import java.text.SimpleDateFormat;
import java.util.List;

import service.BeaconService;
import service.TTService;

public class ScreenBroadcastReceiver extends BroadcastReceiver {

    private BeaconManager beaconManager;

    @Override
    public void onReceive(final Context context, Intent intent) {
        Log.i("[BroadcastReceiver]", "MyReceiver");
        final BeaconService beaconService = new BeaconService(context);
        beaconManager = beaconService.getBeaconManager();
        if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
            Log.i("[BroadcastReceiver]", "Boot Completed");
            context.startService(new Intent(context, TTService.class));

        } else if (intent.getAction().equals(Intent.ACTION_SCREEN_ON)) {
            Log.i("[BroadcastReceiver]", "Screen ON");
            //TODO (buscar beacon)
            final Long timeinit = System.currentTimeMillis();
            final String userName = SharedPreferencesHelper.read(context, AppConstants.SHARED_PREFERENCES_FILE, AppConstants.SHARED_PREFERENCES_USER, "");
            final String userPassword = SharedPreferencesHelper.read(context, AppConstants.SHARED_PREFERENCES_FILE, AppConstants.SHARED_PREFERENCES_PASSWORD, "");
            final String lastCheck = SharedPreferencesHelper.read(context, AppConstants.SHARED_PREFERENCES_FILE, AppConstants.SHARED_PREFERENCES_TIME_CHECK, "");

            if (Utils.checkBeaconLastCheckInOut(userName, userPassword, lastCheck, timeinit, new SimpleDateFormat(AppConstants.DATE_FORMAT))) {
                beaconManager.setRangingListener(new BeaconManager.RangingListener() {
                    @Override
                    public void onBeaconsDiscovered(Region region, final List<Beacon> beacons) {
                        if (beacons.size() > 0) {
                            final Double distance = beaconService.getDistance(beacons.get(0));
                            Log.e("RangingListener", "ACHOU! - dist: " + distance);
                            if (distance <= AppConstants.BEACON_DISTANCE) {
                                doCheck(context);
                            }

                            if ((System.currentTimeMillis() - timeinit) > AppConstants.BEACON_LISTENING_TIME) {
                                doStopManager();
                            }
                        }
                    }
                });

            }
        }
    }


    private void doStopManager(){
        beaconManager.disconnect();
    }

    private void doCheck(final Context context){

        doStopManager();

        Log.e("doCheck", "Disconnected");
        showNotification(context);
    }

    private void showNotification(final Context context) {
        Utils.vibrate(context);
        Log.e("showNotification", "Entrou");
        Intent intentOpen = new Intent(context, MainActivity.class);
        intentOpen.putExtra("acao", "apontar_horas");
        PendingIntent pendingIntentOpen = PendingIntent.getActivity(context, 0, intentOpen, PendingIntent.FLAG_UPDATE_CURRENT);

        Bitmap bmpLargeIcon = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_launcher);
        Notification notif = new NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.ic_launcher)
                .setLargeIcon(bmpLargeIcon)
                .setContentTitle("Time Tracking")
                .setContentText("Deseja fazer o apontamento agora?")
                //.setStyle()
                //.setTicker("Ticker Text")
                .setAutoCancel(true)
                //.setContentIntent(pendingIntentOpen)
                .addAction(R.drawable.ic_launcher, "APONTAR", pendingIntentOpen)
                .build();

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
        notificationManager.notify(0, notif);
    }



}