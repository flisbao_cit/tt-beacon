package com.ciandt.digital.manager;

import com.ciandt.digital.business.TimeTrackingAsynckTask;
import com.ciandt.digital.business.TimeTrackingListener;

/**
 * Created by edgardcardoso on 26/08/14.
 */
public class TimeTrackingServiceImpl implements TimeTrackingService {

    private String user;
    private String password;

    private final String USER_AGENT = "Android";

    @Override
    public TimeTrackingService user(String value) {
        this.user = value;
        return this;
    }

    @Override
    public TimeTrackingService password(String value) {
        this.password = value;
        return this;
    }

    @Override
    public void check(TimeTrackingListener listener) {
        TimeTrackingAsynckTask task = new TimeTrackingAsynckTask(user, password, listener);
        task.execute();
    }



}
