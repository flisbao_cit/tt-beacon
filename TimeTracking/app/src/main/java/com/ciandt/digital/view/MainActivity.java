package com.ciandt.digital.view;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.ciandt.digital.R;
import com.ciandt.digital.business.TimeTrackingListener;
import com.ciandt.digital.constants.AppConstants;
import com.ciandt.digital.helpers.SharedPreferencesHelper;
import com.ciandt.digital.manager.TimeTrackingService;
import com.ciandt.digital.manager.TimeTrackingServiceImpl;
import com.ciandt.digital.receiver.ScreenBroadcastReceiver;
import com.ciandt.digital.utils.Utils;
import com.estimote.sdk.Beacon;
import com.estimote.sdk.BeaconManager;
import com.estimote.sdk.Region;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import service.BeaconService;
import service.TTService;


public class MainActivity extends Activity {

    private Button buttonCheck;
    private EditText editTextUser;
    private EditText editTextPassword;
    private CheckBox checkBox;
    private TextView textviewLastcheck;




    private ScreenBroadcastReceiver screenReciever;

    private BeaconManager beaconManager;
    private SimpleDateFormat sdf = new SimpleDateFormat(AppConstants.DATE_FORMAT);
    private long timeInit = 0;
    private boolean readBeacon = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        editTextUser = (EditText) findViewById(R.id.edittext_user);
        editTextPassword = (EditText) findViewById(R.id.edittext_password);
        buttonCheck = (Button) findViewById(R.id.button_check);
        checkBox = (CheckBox) findViewById(R.id.checkbox);
        textviewLastcheck = (TextView) findViewById(R.id.textview_lastcheck);
        timeInit = System.currentTimeMillis();
        handlerButtonEvent();
        handlerCheckboxEvent();

        readFromPreferences();
        doCheckBeaconManager();
        if (isActionTracking()) {
            //TODO
            // showDialog();
            Toast.makeText(this, getString(R.string.success_msg), Toast.LENGTH_LONG).show();
            doCheckinOut();
        } else {
            startService(new Intent(this, TTService.class));
        }
    }

    private boolean isActionTracking() {
        Intent intent = getIntent();
        return (intent.hasExtra("acao") &&
                intent.getExtras().getString("acao").equalsIgnoreCase("apontar_horas"));
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        return ni != null;
    }

    private void handlerCheckboxEvent(){
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

            }
        });
    }

    private void readFromPreferences(){
        boolean checked = SharedPreferencesHelper.readBoolean(this,AppConstants.SHARED_PREFERENCES_FILE,AppConstants.SHARED_PREFERENCES_CHECKED, false);
        final String userName = SharedPreferencesHelper.read(this,AppConstants.SHARED_PREFERENCES_FILE,AppConstants.SHARED_PREFERENCES_USER,"");
        final String userPassword = SharedPreferencesHelper.read(this, AppConstants.SHARED_PREFERENCES_FILE, AppConstants.SHARED_PREFERENCES_PASSWORD, "");
        if (checked){
            editTextUser.setText(userName);
            editTextPassword.setText(userPassword);
            checkBox.setChecked(true);
        }
        final String lastCheck = SharedPreferencesHelper.read(this, AppConstants.SHARED_PREFERENCES_FILE, AppConstants.SHARED_PREFERENCES_TIME_CHECK, "");
        readBeacon = Utils.checkBeaconLastCheckInOut(userName,userPassword,lastCheck,timeInit,sdf);
        textviewLastcheck.setText(lastCheck);
    }



    private void saveToPreferences(){

        if (checkBox.isChecked()){
            SharedPreferencesHelper.write(this,AppConstants.SHARED_PREFERENCES_FILE,AppConstants.SHARED_PREFERENCES_USER, editTextUser.getText().toString());
            SharedPreferencesHelper.write(this,AppConstants.SHARED_PREFERENCES_FILE,AppConstants.SHARED_PREFERENCES_PASSWORD, editTextPassword.getText().toString());
            SharedPreferencesHelper.write(this,AppConstants.SHARED_PREFERENCES_FILE,AppConstants.SHARED_PREFERENCES_CHECKED, true);
        }else{
            SharedPreferencesHelper.write(this,AppConstants.SHARED_PREFERENCES_FILE,AppConstants.SHARED_PREFERENCES_CHECKED, false);
        }
    }

    private void handlerButtonEvent(){

        buttonCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doCheckinOut();
            }
        });
    }

    private void doCheckinOut(){
        if (!isNetworkConnected()){
            showDialog(getString(R.string.no_connection), false);
            return;
        }

        saveToPreferences();


        buttonCheck.setEnabled(false);

        final ProgressDialog progressDialog = ProgressDialog.show(MainActivity.this, "" ,getString(R.string.progess_logging_dialog_message), true);
        progressDialog.setCancelable(false);


        TimeTrackingService service = new TimeTrackingServiceImpl();
        service.user(editTextUser.getText().toString())
                .password(editTextPassword.getText().toString())
                .check(new TimeTrackingListener() {
                    @Override
                    public void onSucess(String msg, Calendar calendar) {
                        buttonCheck.setEnabled(true);
                        progressDialog.dismiss();


                        String lastCheck = sdf.format(calendar.getTime());
                        SharedPreferencesHelper.write(MainActivity.this, AppConstants.SHARED_PREFERENCES_FILE, AppConstants.SHARED_PREFERENCES_TIME_CHECK, lastCheck);
                        textviewLastcheck.setText(lastCheck);

                        showDialog(msg, true);
                    }

                    @Override
                    public void onError(String msg, Calendar calendar) {
                        buttonCheck.setEnabled(true);
                        progressDialog.dismiss();
                        showDialog(msg, false);
                    }
                });
    }


    private void showDialog(String msg, boolean sucess){
        int icon;

        if (sucess){
            icon = R.drawable.ic_action_about;
        }else{
            icon = R.drawable.ic_alerts_and_states_error;
        }

        new AlertDialog.Builder(this)
                .setTitle(getString(R.string.alert_dialog_title))
                .setMessage(msg)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setIcon(icon)
                .show();
    }




    private void doCheckBeaconManager() {
       if(readBeacon) {
           final BeaconService beaconService = new BeaconService(getApplicationContext());

           if (beaconManager == null) {
               beaconManager = beaconService.getBeaconManager();
           }

           beaconManager.setRangingListener(new BeaconManager.RangingListener() {
               @Override
               public void onBeaconsDiscovered(Region region, final List<Beacon> beacons) {
                   if (beacons.size() > 0) {
                       final Double distance = beaconService.getDistance(beacons.get(0));
                       Log.e("RangingListener", beacons.get(0).toString() + "ACHOU! - dist: " + distance);

                       if (distance <= AppConstants.BEACON_DISTANCE) {
                           Toast.makeText(MainActivity.this, beacons.get(0).toString() + " distance: " + distance, Toast.LENGTH_LONG).show();
                           showDialog();
                       }

                       if ((System.currentTimeMillis() - timeInit) > AppConstants.BEACON_LISTENING_TIME) {
                           doStopManager();
                       }
                   }
               }
           });
       }
    }


    private void showDialog(){
        doStopManager();
        Utils.vibrate(this);
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        alertDialogBuilder.setMessage(getString(R.string.dialog_log_tt_message));
        alertDialogBuilder.setPositiveButton(getString(R.string.dialog_log_tt_positive), new
                DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //Toast.makeText(MainActivity.this, getString(R.string.success_msg), Toast.LENGTH_SHORT).show();
                        doCheckinOut();
                    }
                });
        alertDialogBuilder.setNegativeButton(getString(R.string.dialog_log_tt_negative), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });

        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void doStopManager(){
        beaconManager.disconnect();
    }
}
