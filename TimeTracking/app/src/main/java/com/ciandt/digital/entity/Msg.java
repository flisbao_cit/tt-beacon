package com.ciandt.digital.entity;

/**
 * Created by jfavetti on 03/03/2016.
 */
public class Msg {
    private String msg;
    private int type;
    private int time;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }


}
