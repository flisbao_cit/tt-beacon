package com.ciandt.digital.utils;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.os.Vibrator;

import com.ciandt.digital.constants.AppConstants;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by flisbao on 14/04/2016.
 */
public class Utils {

    public static boolean checkBluetooth() {
        final BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter != null && mBluetoothAdapter.isEnabled()) {
            return true;
        }
        return false;
    }

    public static void vibrate(final Context context){
        final Vibrator v = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        // Vibrate for 500 milliseconds
        v.vibrate(500);
    }

    public static boolean checkBeaconLastCheckInOut(final String userName, final String userPassword , final String date, final Long timeInit, final SimpleDateFormat sdf){

        if(userName.length()>0 && userPassword.length()>0){
            try{
                final Date dtLastCheck = sdf.parse(date);
                if(timeInit - dtLastCheck.getTime() > AppConstants.TIME_TO_CHECKINOUT){
                    return false;
                }

            } catch (ParseException e) {
                return true;
            }
            return true;
        }

        return false;
    }
}
