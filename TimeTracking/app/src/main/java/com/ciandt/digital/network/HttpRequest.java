package com.ciandt.digital.network;

import android.util.Log;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by edgardcardoso on 29/08/14.
 */
public class HttpRequest {


    private static final String URL_TT_CHECK_IN_OUT = "https://tt.ciandt.com/.net/index.ashx/SaveTimmingEvent";
    private static final String URL_TT_TIME = "https://tt.ciandt.com/.net/index.ashx/GetClockDeviceInfo?deviceID=2";


    private HashMap<String, String> params;


    public HttpRequest() {
        params = new HashMap<String, String>();
        params.put("deviceID", "2");
        params.put("eventType", "1");
        params.put("userName", "");
        params.put("password", "");
        params.put("cracha", "");
        params.put("costCenter", "");
        params.put("leave", "");
        params.put("func", "");
        params.put("cdiDispositivoAcesso", "2");
        params.put("cdiDriverDispositivoAcesso", "10");
        params.put("cdiTipoIdentificacaoAcesso", "7");
        params.put("oplLiberarPETurmaRVirtual", "false");
        params.put("cdiTipoUsoDispositivo", "1");
        params.put("qtiTempoAcionamento", "0");
        params.put("d1sEspecieAreaEvento", "Nenhuma");
        params.put("d1sAreaEvento", "Nenhum");
        params.put("d1sSubAreaEvento", "Nenhum(a)");
        params.put("d1sEvento", "Nenhum");
        params.put("oplLiberarFolhaRVirtual", "false");
        params.put("oplLiberarCCustoRVirtual", "false");
        params.put("qtiHorasFusoHorario", "0");
        params.put("cosEnderecoIP", "127.0.0.1");
        params.put("nuiPorta", "7069");
        params.put("oplValidaSenhaRelogVirtual", "false");
        params.put("useUserPwd", "true");
        params.put("useCracha", "false");
        params.put("dtTimeEvent", "");
        params.put("oplLiberarFuncoesRVirtual", "false");
        params.put("sessionID", "0");
        params.put("selectedEmployee", "0");
        params.put("selectedCandidate", "0");
        params.put("selectedVacancy", "0");
        params.put("dtFmt", "d/m/Y");
        params.put("tmFmt", "H:i:s");
        params.put("shTmFmt", "H:i");
        params.put("dtTmFmt", "d/m/Y H:i:s");
        params.put("language", "0");

    }


    public String checkInOut(String user, String password) throws IOException {


        params.put("userName", user);
        params.put("password", password);

        String payload = "";

        for (Object key : params.keySet()) {
            payload = payload + key.toString() + "=" + URLEncoder.encode(params.get(key), "utf8") + "&";
        }


        BufferedReader bufferedReader = null;
        try {
            URL obj = new URL(URL_TT_CHECK_IN_OUT);
            HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();

            //add reuqest header
            con.setRequestMethod("POST");

            con.setRequestProperty("Content-type", "application/x-www-form-urlencoded");
            con.setRequestProperty("Accept","Accept:*/*");
            con.setRequestProperty("Cookie","clockDeviceToken=nHuH/qaEaN1TzYclwDbze2UcjZeQtjjudvHqcjFufA==");
            con.setRequestProperty("Origin","android");
            con.setRequestProperty("User-Agent","Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.94 Safari/537.36");
            con.setConnectTimeout(30000);
            con.setReadTimeout(30000);

            // Send post request
            con.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(payload);
            wr.flush();
            wr.close();


            bufferedReader = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = bufferedReader.readLine()) != null) {
                inputLine = java.net.URLDecoder.decode(inputLine, "UTF-8");
                response.append(inputLine);
            }

            Log.w("LOG", "Response Code : " + response.toString());
            return response.toString();




        } finally {
            bufferedReader.close();
        }
    }


    public Calendar getTime() throws Exception {

        BufferedReader bufferedReader = null;
        try {
            URL obj = new URL(URL_TT_TIME);
            HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();

            //add reuqest header
            con.setRequestMethod("GET");

            con.setRequestProperty("Accept","Accept:*/*");
            con.setRequestProperty("Cookie","clockDeviceToken=nHuH/qaEaN1TzYclwDbze2UcjZeQtjjudvHqcjFufA==");
            con.setRequestProperty("Origin","android");
            con.setRequestProperty("User-Agent","Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.94 Safari/537.36");
            con.setConnectTimeout(30000);
            con.setReadTimeout(30000);

            bufferedReader = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = bufferedReader.readLine()) != null) {
                inputLine = java.net.URLDecoder.decode(inputLine, "UTF-8");
                response.append(inputLine);
            }

            Log.w("LOG", "Response Code : " + response.toString());

            int start = response.toString().indexOf("(");
            int end = response.toString().indexOf(")");

            String string =  response.toString().substring(start+1, end);

            String [] slpit =  string.split(",");

            Calendar calendar = new GregorianCalendar(Integer.decode(slpit[0]),
                    Integer.decode(slpit[1]),
                    Integer.decode(slpit[2]),
                    Integer.decode(slpit[3]),
                    Integer.decode(slpit[4]),
                    Integer.decode(slpit[5]));

            return calendar;
        } finally {
            bufferedReader.close();
        }
    }





}
