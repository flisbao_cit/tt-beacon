package com.ciandt.digital.manager;

import com.ciandt.digital.business.TimeTrackingListener;

/**
 * Created by edgardcardoso on 26/08/14.
 */
public interface TimeTrackingService {


    public TimeTrackingService user(String value);
    public TimeTrackingService password(String value);
    public void check(TimeTrackingListener listener);

}
