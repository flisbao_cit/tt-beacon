package com.ciandt.digital.business;

import java.util.Calendar;

/**
 * Created by edgardcardoso on 27/08/14.
 */
public interface TimeTrackingListener {

    public void onSucess(String msg, Calendar calendar);
    public void onError(String msg, Calendar calendar);
}
