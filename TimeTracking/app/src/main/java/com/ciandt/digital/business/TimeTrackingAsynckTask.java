package com.ciandt.digital.business;

import android.os.AsyncTask;

import com.ciandt.digital.entity.CallBack;
import com.ciandt.digital.exception.TimeTrackingException;
import com.ciandt.digital.network.HttpRequest;
import com.google.gson.Gson;

import java.util.Calendar;

/**
 * Created by edgardcardoso on 27/08/14.
 */
public class TimeTrackingAsynckTask extends AsyncTask<Void, Void, TimeTrackingAsynckTask.Response> {


    private String user;
    private String password;
    private TimeTrackingListener listener;

    private TimeTrackingException trackingException;

    private static final int CHECK_RECORDED = 1;
    private static final int INVALID_CREDENTIALS=2;
    private static final int OFFLINE_BACKEND = 3;

    public TimeTrackingAsynckTask(String user, String password, TimeTrackingListener listener) {
        this.user = user;
        this.password = password;
        this.listener = listener;
    }




    @Override
    protected Response doInBackground(Void[] params) {

        try {
            HttpRequest httpRequest = new HttpRequest();
            Response response = new Response();
            response.calendar = httpRequest.getTime();
            String json = httpRequest.checkInOut(user, password);
            response.callback = new Gson().fromJson(json, CallBack.class);
            return response;
        } catch (Exception e) {
            trackingException = new TimeTrackingException(e.getMessage());
            //Log.e("Error", e.getMessage());
            return null;
        }

    }

    @Override
    protected void onPostExecute(Response response) {
        if (trackingException != null) {
            listener.onError("Internal error:" + trackingException.getMessage(), response.calendar);
        }else if (!response.callback.isSuccess()) {
            listener.onError(response.callback.getErrorDetail(), response.calendar);
        } else {
            if (response.callback.getMsg().getType() == CHECK_RECORDED) {
                listener.onSucess(response.callback.getMsg().getMsg(), response.calendar);
            }
            else if (response.callback.getMsg().getType() == INVALID_CREDENTIALS) {
                listener.onError("Server Error: Invalid User/Password", response.calendar);
            }
            else if (response.callback.getMsg().getType() == OFFLINE_BACKEND) {
                listener.onError("Server Error: Backend out of service", response.calendar);
            }
            else{
                listener.onError("Server Error: Unknow", response.calendar);
            }
        }
    }

    class Response{
        CallBack callback;
        Calendar calendar;
    }
}
