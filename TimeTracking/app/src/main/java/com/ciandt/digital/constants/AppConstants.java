package com.ciandt.digital.constants;

/**
 * Created by flisbao on 14/04/2016.
 */
public class AppConstants {

    public final static Double BEACON_DISTANCE = 7.0d;
    public final static Long BEACON_LISTENING_TIME = (1L*60L*1000L);
    public final static String DATE_FORMAT = "dd/MM/yyyy - HH:mm";
    public final static long BEACON_SCAN_PERIOD = 1*1000L;
    public final static long BEACON_TIMEOUT = 500L;
    public final static long TIME_TO_CHECKINOUT = (3L*60L*60L*1000L);

    public final static String SHARED_PREFERENCES_USER = "user";
    public final static String SHARED_PREFERENCES_PASSWORD = "password";
    public final static String SHARED_PREFERENCES_CHECKED = "checked";
    public final static String SHARED_PREFERENCES_TIME_CHECK = "time";
    public final static String SHARED_PREFERENCES_FILE = "com.ciandt.tt";
}
