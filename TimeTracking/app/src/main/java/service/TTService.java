package service;

import android.app.Service;
import android.content.*;
import android.os.*;
import com.ciandt.digital.receiver.ScreenBroadcastReceiver;

public class TTService extends Service {
    private IntentFilter screenStateFilter = null;
    private Context context = this;
    private ScreenBroadcastReceiver screenReceiver = null;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        screenReceiver = new ScreenBroadcastReceiver();
        registerScreenReciever();
    }

    public void registerScreenReciever(){
        screenStateFilter = new IntentFilter();
        screenStateFilter.addAction(Intent.ACTION_SCREEN_ON);
        context.getApplicationContext().registerReceiver(screenReceiver, screenStateFilter);
    }

    public void unRegisterReciever(){
        context.unregisterReceiver(screenReceiver);
    }
}