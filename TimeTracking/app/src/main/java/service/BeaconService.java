package service;

import android.content.Context;
import android.util.Log;

import com.ciandt.digital.constants.AppConstants;
import com.ciandt.digital.utils.Utils;
import com.estimote.sdk.Beacon;
import com.estimote.sdk.BeaconManager;
import com.estimote.sdk.Region;

import java.util.List;
import java.util.UUID;

/**
 * Created by flisbao on 14/04/2016.
 */
public class BeaconService {

    private Context context;
    private BeaconManager beaconManager;
    private Region region;

    public BeaconService(final Context context){
        this.context = context;
    }


    public double getDistance(final Beacon beacon){
        return com.estimote.sdk.Utils.computeAccuracy(beacon);
    }




    public BeaconManager getBeaconManager(){
        beaconManager = new BeaconManager(context);
        region = new Region(
                "monitored region",
                UUID.fromString("B9407F30-F5F8-466E-AFF9-25556B57FE6D"),
                null, null);

        beaconManager.stopMonitoring(region);
        beaconManager.stopRanging(region);
        beaconManager.disconnect();
        beaconManager.connect(new BeaconManager.ServiceReadyCallback() {
            @Override
            public void onServiceReady() {
                beaconManager.startMonitoring(region);
                beaconManager.startRanging(region);
            }
        });
        beaconManager.setBackgroundScanPeriod(AppConstants.BEACON_SCAN_PERIOD, AppConstants.BEACON_TIMEOUT);

        return beaconManager;
    }


    public boolean found(){

        final boolean[] read = {false};

        if (Utils.checkBluetooth() && context !=null) {
            beaconManager = new BeaconManager(context);
            region = new Region(
                    "monitored region",
                    UUID.fromString("B9407F30-F5F8-466E-AFF9-25556B57FE6D"),
                    null, null);

            /*beaconManager.setMonitoringListener(new BeaconManager.MonitoringListener() {
                @Override
                public void onEnteredRegion(Region region, List<Beacon> list) {
                    Log.e("onEnteredRegion", "ENTROU NA REGIAO");
                }

                @Override
                public void onExitedRegion(Region region) {
                    Log.e("onEnteredRegion", "SAIU DA REGIAO");
                }
            });*/

            beaconManager.setRangingListener(new BeaconManager.RangingListener() {
                @Override
                public void onBeaconsDiscovered(Region region, final List<Beacon> beacons) {
                    if (beacons.size() > 0) {
                        Log.e("RangingListener", "ACHOU! - dist: " + getDistance(beacons.get(0)));
                        read[0] = true;
                    }
                }
            });

            beaconManager.stopMonitoring(region);
            beaconManager.stopRanging(region);
            beaconManager.disconnect();
            beaconManager.connect(new BeaconManager.ServiceReadyCallback() {
                @Override
                public void onServiceReady() {
                    beaconManager.startMonitoring(region);
                    beaconManager.startRanging(region);
                }
            });


        }

        return read[0];
    }


}
